//
//  GameScene.swift
//  BirdShoot
//
//  Created by Balwinder Singh on 2018-10-09.
//  Copyright © 2018 Balwinder Singh. All rights reserved.
//
import SpriteKit

class GameScene: SKScene {
    //let enemy = SKSpriteNode(imageNamed: "bird1")
    let zombie = SKSpriteNode(imageNamed: "zombie1")
    var lastUpdateTime: TimeInterval = 0
    var dt: TimeInterval = 0
    let gun = SKSpriteNode(imageNamed: "gun1")
    let zombieMovePointsPerSec: CGFloat = 480.0
    var velocity = CGPoint(x: 400, y: 0)
    let playableRect: CGRect
    var lastTouchLocation: CGPoint?
    let zombieRotateRadiansPerSec:CGFloat = 4.0 * π
    var zombieAnimation: SKAction
    var gunAnimation: SKAction
    let catCollisionSound: SKAction = SKAction.playSoundFileNamed(
        "shoot.wav", waitForCompletion: false)
    let enemyCollisionSound: SKAction = SKAction.playSoundFileNamed(
        "bird.wav", waitForCompletion: false)
    var invincible = false
    var birdinvincible = false
    let catMovePointsPerSec:CGFloat = 480.0
    var lives = 5
    var gameOver = false
    let cameraNode = SKCameraNode()
    let cameraMovePointsPerSec: CGFloat = 200.0
    
    
    override init(size: CGSize) {
        let maxAspectRatio:CGFloat = 16.0/9.0
        let playableHeight = size.width / maxAspectRatio
        let playableMargin = (size.height-playableHeight)/2.0
        playableRect = CGRect(x: 0, y: playableMargin,
                              width: size.width,
                              height: playableHeight)
        
        // 1
        var textures:[SKTexture] = []
        // 2
        for i in 1...3 {
            textures.append(SKTexture(imageNamed: "bird\(i)"))
        }
        // 3
        textures.append(textures[2])
        textures.append(textures[1])
        
        // 4
        zombieAnimation = SKAction.animate(with: textures,
                                           timePerFrame: 0.1)
        
        var textures1:[SKTexture] = []
        // 2
        for i in 1...2 {
            textures1.append(SKTexture(imageNamed: "gun\(i)"))
        }
        // 3
        //textures1.append(textures1[2])
        textures1.append(textures1[0])
        gunAnimation = SKAction.animate(with: textures1,
                                        timePerFrame: 0.1)
        super.init(size: size)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func debugDrawPlayableArea() {
        let shape = SKShapeNode()
        let path = CGMutablePath()
        path.addRect(playableRect)
        shape.path = path
        shape.strokeColor = SKColor.red
        shape.lineWidth = 4.0
        addChild(shape)
    }
    
    override func didMove(to view: SKView) {
        
        playBackgroundMusic(filename: "background.wav")
        
        for i in 0...1 {
            let background = backgroundNode()
            background.anchorPoint = CGPoint.zero
            background.position =
                CGPoint(x: CGFloat(i)*background.size.width, y: 0)
            background.name = "background"
            background.zPosition = -1
            addChild(background)
        }
        
        zombie.position = CGPoint(x: 0, y: 1000)
        zombie.zPosition = 0
        addChild(zombie)
        gun.position = CGPoint(x:1040, y: 290)
        addChild(gun)
        
        // zombie.run(SKAction.repeatForever(zombieAnimation))
        
        //    run(SKAction.repeatForever(
        //
        //        //enemy
        //      SKAction.sequence([SKAction.run() { [weak self] in
        //        self?.move(sprite: (self?.zombie)!, velocity: (self?.velocity)!)
        //                    },
        //                         SKAction.wait(forDuration: 0.2)])));
        
        //    run(SKAction.repeatForever(
        //      SKAction.sequence([SKAction.run() { [weak self] in
        //        self?.spawnEnemy()
        //                        },
        //                        SKAction.wait(forDuration: 2.0)])))
        
        // debugDrawPlayableArea()
        
        addChild(cameraNode)
        camera = cameraNode
        cameraNode.position = CGPoint(x: size.width/2, y: size.height/2)
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        move(sprite: zombie, velocity: velocity)
        
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        } else {
            dt = 0
        }
        lastUpdateTime = currentTime
        
        boundsCheckZombie()
        // checkCollisions()
        //moveTrain()
        // moveCamera()
        
        if lives <= 0 && !gameOver {
            gameOver = true
            print("You lose!")
            backgroundMusicPlayer.stop()
            
            // 1
            let gameOverScene = GameOverScene(size: size, won: false)
            gameOverScene.scaleMode = scaleMode
            // 2
            let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
            // 3
            view?.presentScene(gameOverScene, transition: reveal)
        }
        
        // cameraNode.position = zombie.position
        
    }
    
    
    
    
    
    func move(sprite: SKSpriteNode, velocity: CGPoint) {
        if(birdinvincible==true){
            
            let amountToMove = CGPoint(x: velocity.x * CGFloat(dt)+8,
                                       y: velocity.y * CGFloat(dt))
            
            sprite.position = CGPoint(x: size.width/2, y: size.height/6)
            birdinvincible = false
            zombie.position = CGPoint(x: 0, y: 1000)
            
        }
        if(birdinvincible==false){
            // print("else")
            
            startZombieAnimation()
            let amountToMove = CGPoint(x: velocity.x * CGFloat(dt)+4,
                                       y: velocity.y * CGFloat(dt))
            
            sprite.position += amountToMove
            
        }
    }
    
    //  func moveZombieToward(location: CGPoint) {
    //    startZombieAnimation()
    //    let offset = location - zombie.position
    //    let direction = offset.normalized()
    //    velocity = direction * zombieMovePointsPerSec
    //  }
    //
    func sceneTouched(touchLocation:CGPoint) {
        //    lastTouchLocation = touchLocation
        //    moveZombieToward(location: touchLocation)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        run(catCollisionSound)
        spawnEnemy()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        // let touchLocation = touch.location(in: self)
        //  sceneTouched(touchLocation: touchLocation)
      
        run(catCollisionSound)
        spawnEnemy()
    }
    
    func boundsCheckZombie() {
        let bottomLeft = CGPoint(x: cameraRect.minX, y: cameraRect.minY)
        let topRight = CGPoint(x: cameraRect.maxX, y: cameraRect.maxY)
        //    let left = CGPoint(x:cameraRect.maxX,y:400)
        //    if zombie.position == left {
        //        zombie.position =  CGPoint(x:400,y:400)
        //    }
        if zombie.position.x <= bottomLeft.x {
            print(bottomLeft)
            
            zombie.position.x = bottomLeft.x
            velocity.x = abs(velocity.x)
            
        }
        if zombie.position.x >= topRight.x {
            
            zombie.position.x = bottomLeft.x
            velocity.x = abs(velocity.x)
            //      zombie.position.x = topRight.x
            //      velocity.x = -velocity.x
            
        }
        if zombie.position.y <= bottomLeft.y {
            zombie.position.y = bottomLeft.y
            velocity.y = -velocity.y
            print("3")
        }
        if zombie.position.y >= topRight.y {
            zombie.position.y = topRight.y
            velocity.y = -velocity.y
            print("4")
        }
    }
    
    func rotate(sprite: SKSpriteNode, direction: CGPoint, rotateRadiansPerSec: CGFloat) {
        let shortest = shortestAngleBetween(angle1: sprite.zRotation, angle2: velocity.angle)
        let amountToRotate = min(rotateRadiansPerSec * CGFloat(dt), abs(shortest))
        sprite.zRotation += shortest.sign() * amountToRotate
    }
    
    func spawnEnemy() {
        startgunAnimation()
        
        let enemy = SKSpriteNode(imageNamed: "bullet")
        enemy.setScale(2)
        //    cat.position = CGPoint(
        //        x: CGFloat.random(min: cameraRect.minX,
        //                          max: cameraRect.maxX),
        //        y: CGFloat.random(min: cameraRect.minY,
        //                          max: cameraRect.maxY))
        enemy.position = CGPoint(
            x: 1000,
            y: 400)
        enemy.zPosition = 50
        enemy.name = "enemy"
        addChild(enemy)
        
        let actionMove =
            SKAction.moveBy(x:0 , y: (size.width + enemy.size.width), duration: 2.0)
        let actionRemove = SKAction.removeFromParent()
        enemy.run(SKAction.sequence([actionMove, actionRemove]))
        
    }
    
    func startZombieAnimation() {
        if zombie.action(forKey: "animation") == nil {
            zombie.run(
                SKAction.repeatForever(zombieAnimation),
                withKey: "animation")
        }
    }
    
    func stopZombieAnimation() {
        zombie.removeAction(forKey: "animation")
    }
    
    
    func startgunAnimation() {
        if gun.action(forKey: "animation1") == nil {
            gun.run(
                SKAction.repeat(gunAnimation, count: 1),
                withKey: "animation1")
        }
    }
    
    func stopGunAnimatiomn() {
        gun.removeAction(forKey: "animation1")
    }
    
    //  func spawnCat() {
    //    // 1
    //    let cat = SKSpriteNode(imageNamed: "cat")
    //    cat.name = "cat"
    //    cat.position = CGPoint(
    //      x: CGFloat.random(min: cameraRect.minX,
    //                        max: cameraRect.maxX),
    //      y: CGFloat.random(min: cameraRect.minY,
    //                        max: cameraRect.maxY))
    //    cat.zPosition = 50
    //    cat.setScale(0)
    //    addChild(cat)
    //    // 2
    //    let appear = SKAction.scale(to: 1.0, duration: 0.5)
    //
    //    cat.zRotation = -π / 16.0
    //    let leftWiggle = SKAction.rotate(byAngle: π/8.0, duration: 0.5)
    //    let rightWiggle = leftWiggle.reversed()
    //    let fullWiggle = SKAction.sequence([leftWiggle, rightWiggle])
    //
    //    let scaleUp = SKAction.scale(by: 1.2, duration: 0.25)
    //    let scaleDown = scaleUp.reversed()
    //    let fullScale = SKAction.sequence(
    //      [scaleUp, scaleDown, scaleUp, scaleDown])
    //    let group = SKAction.group([fullScale, fullWiggle])
    //    let groupWait = SKAction.repeat(group, count: 10)
    //
    //    let disappear = SKAction.scale(to: 0, duration: 0.5)
    //    let removeFromParent = SKAction.removeFromParent()
    //    let actions = [appear, groupWait, disappear, removeFromParent]
    //    cat.run(SKAction.sequence(actions))
    //  }
    
    //  func zombieHit(cat: SKSpriteNode) {
    //    cat.name = "train"
    //    cat.removeAllActions()
    //    cat.setScale(1.0)
    //    cat.zRotation = 0
    //
    //    let turnGreen = SKAction.colorize(with: SKColor.green, colorBlendFactor: 1.0, duration: 0.2)
    //    cat.run(turnGreen)
    //
    //    run(catCollisionSound)
    //  }
    
    func zombieHit(enemy: SKSpriteNode) {
        invincible = true
        let blinkTimes = 1.0
        let duration = 0.5
        let blinkAction = SKAction.customAction(withDuration: duration) { node, elapsedTime in
            let slice = duration / blinkTimes
            let remainder = Double(elapsedTime).truncatingRemainder(
                dividingBy: slice)
            node.isHidden = remainder > slice / 2
        }
        let setHidden = SKAction.run() { [weak self] in
            self?.zombie.isHidden = false
            self?.invincible = false
        }
        zombie.run(SKAction.sequence([blinkAction, setHidden]))
        birdinvincible = true
        run(enemyCollisionSound)
        
        //loseCats()
        lives -= 1
    }
    
    func checkCollisions() {
        var hitCats: [SKSpriteNode] = []
        enumerateChildNodes(withName: "cat") { node, _ in
            let cat = node as! SKSpriteNode
            if cat.frame.intersects(self.zombie.frame) {
                hitCats.append(cat)
            }
        }
        //
        //    for cat in hitCats {
        //      zombieHit(cat: cat)
        //    }
        
        if invincible {
            return
        }
        
        var hitEnemies: [SKSpriteNode] = []
        enumerateChildNodes(withName: "enemy") { node, _ in
            let enemy = node as! SKSpriteNode
            if node.frame.insetBy(dx: 20, dy: 20).intersects(
                self.zombie.frame) {
                hitEnemies.append(enemy)
            }
        }
        for enemy in hitEnemies {
            zombieHit(enemy: enemy)
        }
    }
    
    override func didEvaluateActions() {
        checkCollisions()
    }
    
    //  func moveTrain() {
    //
    //    var trainCount = 0
    //    var targetPosition = zombie.position
    //
    //    enumerateChildNodes(withName: "train") { node, stop in
    //      trainCount += 1
    //      if !node.hasActions() {
    //        let actionDuration = 0.3
    //        let offset = targetPosition - node.position
    //        let direction = offset.normalized()
    //        let amountToMovePerSec = direction * self.catMovePointsPerSec
    //        let amountToMove = amountToMovePerSec * CGFloat(actionDuration)
    //        let moveAction = SKAction.moveBy(x: amountToMove.x, y: amountToMove.y, duration: actionDuration)
    //        node.run(moveAction)
    //      }
    //      targetPosition = node.position
    //    }
    //
    //    if trainCount >= 15 && !gameOver {
    //      gameOver = true
    //      print("You win!")
    //      backgroundMusicPlayer.stop()
    //
    //      // 1
    //      let gameOverScene = GameOverScene(size: size, won: true)
    //      gameOverScene.scaleMode = scaleMode
    //      // 2
    //      let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
    //      // 3
    //      view?.presentScene(gameOverScene, transition: reveal)
    //    }
    //
    //  }
    //
    //  func loseCats() {
    //    // 1
    //    var loseCount = 0
    //    enumerateChildNodes(withName: "train") { node, stop in
    //      // 2
    //      var randomSpot = node.position
    //      randomSpot.x += CGFloat.random(min: -100, max: 100)
    //      randomSpot.y += CGFloat.random(min: -100, max: 100)
    //      // 3
    //      node.name = ""
    //      node.run(
    //        SKAction.sequence([
    //          SKAction.group([
    //            SKAction.rotate(byAngle: π*4, duration: 1.0),
    //            SKAction.move(to: randomSpot, duration: 1.0),
    //            SKAction.scale(to: 0, duration: 1.0)
    //            ]),
    //          SKAction.removeFromParent()
    //        ]))
    //      // 4
    //      loseCount += 1
    //      if loseCount >= 2 {
    //        stop[0] = true
    //      }
    //    }
    //  }
    
    func backgroundNode() -> SKSpriteNode {
        // 1
        let backgroundNode = SKSpriteNode()
        backgroundNode.anchorPoint = CGPoint.zero
        backgroundNode.name = "background"
        
        // 2
        let background1 = SKSpriteNode(imageNamed: "background1")
        background1.anchorPoint = CGPoint.zero
        background1.position = CGPoint(x: 0, y: 0)
        backgroundNode.addChild(background1)
        
        // 3
        let background2 = SKSpriteNode(imageNamed: "background2")
        background2.anchorPoint = CGPoint.zero
        background2.position =
            CGPoint(x: background1.size.width, y: 0)
        backgroundNode.addChild(background2)
        
        // 4
        backgroundNode.size = CGSize(
            width: background1.size.width + background2.size.width,
            height: background1.size.height)
        return backgroundNode
    }
    
    //  func moveCamera() {
    //    let backgroundVelocity =
    //      CGPoint(x: cameraMovePointsPerSec, y: 0)
    //    let amountToMove = backgroundVelocity * CGFloat(dt)
    //    cameraNode.position += amountToMove
    //
    //    enumerateChildNodes(withName: "background") { node, _ in
    //      let background = node as! SKSpriteNode
    //      if background.position.x + background.size.width <
    //          self.cameraRect.origin.x {
    //        background.position = CGPoint(
    //          x: background.position.x + background.size.width*2,
    //          y: background.position.y)
    //      }
    //    }
    //
    //  }
    
    var cameraRect : CGRect {
        let x = cameraNode.position.x - size.width/2
            + (size.width - playableRect.width)/2
        let y = cameraNode.position.y - size.height/2
            + (size.height - playableRect.height)/2
        return CGRect(
            x: x,
            y: y,
            width: playableRect.width,
            height: playableRect.height)
    }
    
}
